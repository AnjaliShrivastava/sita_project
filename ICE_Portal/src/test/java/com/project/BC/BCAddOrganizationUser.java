package com.project.BC;


import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.project.pages.AddOrganizationUserObject;
import com.project.pages.GeneralObjects;
import com.project.pages.AttachDocumentObjects;
import com.project.pages.SearchOrganizationUser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCAddOrganizationUser extends TestBase {

	public void navigate_To_AddOrganizationUser(ExtentTest logger ) throws IOException {
		mouseHoveronElementAndClick("//a/span[contains(text(),'"+getLocalData("menu.ice.portal", data)+"')]");
		//mouseHoveronElementAndClick(GeneralObjects.linkIcePortal);
        mouseHoveronElementAndClick(GeneralObjects.linkAddOrgUser);
		}

	public void add_User_Details(Map<String, String> data, ExtentTest logger ) throws InterruptedException {
		SelectDropdownByValue(AddOrganizationUserObject.userLevelDropDown, data.get("Level").toUpperCase().trim().replace(' ','_' ), logger);

		if(!data.get("Level").equalsIgnoreCase("Master User") && data.get("Level")!= null) {

		SelectDropdownByValue(AddOrganizationUserObject.orgNamelDropDown , data.get("OrganizationName"), logger);
		}
		if(data.get("Level").equalsIgnoreCase("System User") ||data.get("Level").equalsIgnoreCase("Branch Admin")&& data.get("Level")!= null) {

			SelectDropdownByValue(AddOrganizationUserObject.branchCodelDropDown , data.get("BranchCode"), logger);
			}

		ClickSendText1(AddOrganizationUserObject.GivenName, data.get("GivenName"), logger);
		ClickSendText1(AddOrganizationUserObject.FamilyName, data.get("FamilyName"), logger);
		ClickSendText1(AddOrganizationUserObject.Email, data.get("Email"), logger);
		ClickSendText1(AddOrganizationUserObject.PhoneNumber, data.get("PhoneNo"), logger);
		ClickSendText1(AddOrganizationUserObject.Facsimile, data.get("Facsimile"), logger);
		if(!data.get("Level").equalsIgnoreCase("Master User") && data.get("Level")!= null) {
		ClickSendText1(AddOrganizationUserObject.jobTitle, data.get("JobTitle"), logger);

} }

	public void add_Document(Map<String, String> data, ExtentTest logger ) throws InterruptedException {


		click(AddOrganizationUserObject.addDocPlusBtn, logger);
		SelectDropdownByValue(AddOrganizationUserObject.documentTypeDropdown, data.get("DocumentType").toUpperCase().trim().replace(' ','_' ), logger);
		//SelectDropdownByValue(AddOrganizationUserObject.documentTypeDropdown, data.get("DocumentType"), logger);
		ClickSendText1(AddOrganizationUserObject.documentNo, data.get("DocNo"), logger);
		ClickSendText1(AddOrganizationUserObject.docissueCountry, data.get("IssueCountry"), logger);
		ClickSendText1(AddOrganizationUserObject.Nationality, data.get("Nationality"), logger);
		SelectDropdownByValue(AddOrganizationUserObject.genderDropdown , data.get("Gender"), logger);
		selectCalendarDateMultiLang(AddOrganizationUserObject.DOBcalIcon, GeneralObjects.yearDD, GeneralObjects.monthDD, GeneralObjects.dateObj, data.get("DOB"), logger);
		selectCalendarDateMultiLang(AddOrganizationUserObject.DocExpCalIcon, GeneralObjects.yearDD, GeneralObjects.monthDD, GeneralObjects.dateObj, data.get("ExpDate"), logger);


}

	public void add_Doc_Action( ExtentTest logger ) throws InterruptedException {
     //click(AddOrganizationUserObject.adddocumentBtn);
     SubmitButton(AddOrganizationUserObject.adddocumentBtn, logger);
	}

	public void verify_doc_Rows(Map<String, String> data, ExtentTest logger) {

if(data.get("Arabian").equalsIgnoreCase("No")) {
		String [] docDetails = {data.get("DocumentType"), data.get("DocNo"),data.get("IssueCountry"),data.get("ExpDate"),data.get("DOB"),data.get("Nationality"),  data.get("Gender")  } ;
		verifyElementInTableRow(AddOrganizationUserObject.docRow, docDetails, logger);
}else {
	Map<String, String> DocType = new HashMap<String, String>();
	DocType.put("Permanent Resident", "مقيم دائم");
	DocType.put("Facilitation Document", "وثيقة التسهيلات");
	DocType.put("Identity Card", "بطاقة هوية");
	DocType.put("Military ID", "رقم الجيش");
	DocType.put("None", "لا شيء");
	DocType.put("Other", "لا شيء");
	DocType.put("Passport", "جواز سفر");
	DocType.put("Travel Document", "وثيقة السفر");
	DocType.put("Unknown", "غير معروف");
	DocType.put("Visa", "تأشيرة");
	DocType.put("Alien Registration", "التسجيل الأجنبي");


	String [] docDetails = {DocType.get(data.get("DocumentType")), data.get("DocNo"),data.get("IssueCountry"),reverseDateFormate(data.get("ExpDate")),reverseDateFormate(data.get("DOB")),data.get("Nationality"),  data.get("Gender")  } ;
	verifyElementInTableRow(AddOrganizationUserObject.docRow, docDetails, logger);

}
	}

	public void resetAll_doc_Action( ExtentTest logger ) throws InterruptedException {
		SubmitButton(AddOrganizationUserObject.docResetAllbtn, logger);
		}

	public void add_User(ExtentTest logger) {
		 SubmitButton(AddOrganizationUserObject.addUserBtn, logger);
		 verifyElementText(AddOrganizationUserObject.AddedUserpopup, "IB1027I: Added successfully. Would you like to add another?", "Added user popup is displaying " ,  logger);

	}
	public void No_AdditionalUser(ExtentTest logger) throws InterruptedException, IOException {
		  click(AddOrganizationUserObject.noBtn,  logger);
		 //SubmitButton(AddOrganizationUserObject.noBtn, logger);
		 //verifyElementDisplayed(SearchOrganizationUser.PageTitle, logger);
		 verifyElementDisplayed("//h1[@class='pageTitle' and contains(text(),'"+getLocalData("menu.ice.search.organisation.user",data).trim()+"')]", logger);


	}

	public void validate_resettedField(ExtentTest logger) throws InterruptedException {
		if(data.get("Arabian").equalsIgnoreCase("No")) {
		verifyElementText(AddOrganizationUserObject.documentTypeDropdown, "Alien Registration", "Field value is as expected" , logger);
		}else {
			verifyElementText(AddOrganizationUserObject.documentTypeDropdown, "التسجيل الأجنبي", "Field value is as expected" , logger);
		}
		verifyElementText(AddOrganizationUserObject.docissueCountry, "", "Field value is as expected" , logger);
		verifyElementText(AddOrganizationUserObject.DocExpCalIcon, "", "Field value is as expected" , logger);
		verifyElementText(AddOrganizationUserObject.DOBcalIcon, "", "Field value is as expected" , logger);
		verifyElementText(AddOrganizationUserObject.Nationality, "", "Field value is as expected" , logger);
		verifyElementText(AddOrganizationUserObject.genderDropdown, "Male", "Field value is as expected" , logger);
	}




}