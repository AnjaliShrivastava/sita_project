package com.project.BC;


import java.io.IOException;
import java.util.Date;
import java.util.Map;

import com.project.pages.AddOrganizationUserObject;
import com.project.pages.GeneralObjects;
import com.project.pages.AttachDocumentObjects;
import com.project.pages.SearchOrganizationUser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCAttachDocument extends TestBase {


	public void navigate_To_SearchOrganizationUser(ExtentTest logger ) throws IOException {
		mouseHoveronElementAndClick("//a/span[contains(text(),'"+getLocalData("menu.ice.portal", data)+"')]");
		//mouseHoveronElementAndClick(GeneralObjects.linkIcePortal);
        mouseHoveronElementAndClick(GeneralObjects.linksearchOrg);
		}

	public void Enter_Branch_Details(Map<String, String> data, ExtentTest logger ) throws InterruptedException {

        ClickSendText1(AttachDocumentObjects.BranchName, data.get("BranchName"), logger);
        ClickSendText1(AttachDocumentObjects.BranchLicenseNo, data.get("LicenseNo"), logger);
        ClickSendText1(AttachDocumentObjects.ZoneName, data.get("ZoneName"), logger);
        ClickSendText1(AttachDocumentObjects.LocationSearch, data.get("Location"), logger);
        ClickSendText1(AttachDocumentObjects.BranchEmail, data.get("Email"), logger);
		ClickSendText1(AttachDocumentObjects.BranchPhoneNO, data.get("PhoneNo"), logger);

	}

	public void Attach_Document(Map<String, String> data, ExtentTest logger ) throws InterruptedException {
        Thread.sleep(3000);
		Upload_File(AttachDocumentObjects.FirstAttachFile,data.get("Documentpath"), logger);
}

	public void add_Branch_Action( ExtentTest logger ) throws InterruptedException {
     //click(AddOrganizationUserObject.adddocumentBtn);
     SubmitButton(AttachDocumentObjects.btnAdd, logger);
	}

	public void verify_doc_Rows(Map<String, String> data, ExtentTest logger) {


		String [] docDetails = {data.get("DocumentType"), data.get("DocNo"),data.get("IssueCountry"),data.get("ExpDate"),data.get("DOB"),data.get("Nationality"),  data.get("Gender")  } ;
		verifyElementInTableRow(AddOrganizationUserObject.docRow, docDetails, logger);

	}

	public void resetAll_doc_Action( ExtentTest logger ) throws InterruptedException {
		SubmitButton(AddOrganizationUserObject.docResetAllbtn, logger);
		}

	public void SearchOrg_Results(ExtentTest logger) {
		 SubmitButton(GeneralObjects.searchBtn, logger);
		 click(AttachDocumentObjects.searchOrgResults, logger);
		 SubmitButton(AttachDocumentObjects.addBranchBtn, logger);
	}

	public void No_AdditionalBranch(ExtentTest logger) {
		  click(AddOrganizationUserObject.noBtn,  logger);
		 //SubmitButton(AddOrganizationUserObject.noBtn, logger);
		 verifyElementDisplayed(AttachDocumentObjects.SearchBranchTitle, logger);

	}



public void update_User(ExtentTest logger) throws InterruptedException {

	 SubmitButton(SearchOrganizationUser.saveUserBtn, logger);
	 SubmitButton(SearchOrganizationUser.OKBtn, logger);
	 verifyElementText(SearchOrganizationUser.SuccessfulupdateMsg, "IB1214I: User details have been updated successfully.", "" ,  logger);
	 SubmitButton(SearchOrganizationUser.updatepopupOKBtn, logger);
     SubmitButton(SearchOrganizationUser.CloseBtn, logger);
}

public void delete_doc(ExtentTest logger) {
	click(AddOrganizationUserObject.docRow, logger);
	SubmitButton(SearchOrganizationUser.editBtn, logger);
	SubmitButton(SearchOrganizationUser.deleteDocIcon, logger);
	elementNotDisplayed(AddOrganizationUserObject.docRow, logger);

}




}