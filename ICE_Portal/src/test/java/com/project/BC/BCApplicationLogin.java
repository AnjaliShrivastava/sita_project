package com.project.BC;




import com.project.pages.GeneralObjects;

import com.relevantcodes.extentreports.ExtentTest;




public class BCApplicationLogin extends TestBase {


	public void LogintoApplication( ExtentTest logger ) {

		ClickSendText1(GeneralObjects.txtUsername, getProperty("Username"), logger);
		ClickSendText1(GeneralObjects.txtPassword, getProperty("Password"), logger);
		click(GeneralObjects.btnLogin,logger);

	}



}