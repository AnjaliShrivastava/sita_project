package com.project.BC;


import java.util.Date;
import java.util.Map;

import com.project.pages.AddOrganizationUserObject;
import com.project.pages.GeneralObjects;
import com.project.pages.AttachDocumentObjects;
import com.project.pages.SearchOrganizationUser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCSearchOrganizationUser extends TestBase {


	public void navigate_To_SearchOrganizationUser(ExtentTest logger ) {

		mouseHoveronElementAndClick(GeneralObjects.linkIcePortal);
        mouseHoveronElementAndClick(GeneralObjects.linksearchOrgUser);
		}

	public void Enter_Search_Details(Map<String, String> data, ExtentTest logger ) throws InterruptedException {

		SelectDropdownByVisibleText(AddOrganizationUserObject.userLevelDropDown, data.get("Level"), logger);
        ClickSendText1(AddOrganizationUserObject.GivenName, data.get("GivenName"), logger);
		ClickSendText1(AddOrganizationUserObject.FamilyName, data.get("FamilyName"), logger);

	}

	public void Edit_Document(Map<String, String> data, ExtentTest logger ) throws InterruptedException {


		//click(AddOrganizationUserObject.docRow, logger);

		SubmitButton(SearchOrganizationUser.editBtn, logger);
		click(SearchOrganizationUser.editDocIcon, logger);
		SelectDropdownByVisibleText(AddOrganizationUserObject.documentTypeDropdown, data.get("DocumentType"), logger);
		//SelectDropdownByValue(AddOrganizationUserObject.documentTypeDropdown, data.get("DocumentType"), logger);
		ClickSendText(AddOrganizationUserObject.documentNo, data.get("DocNo"), logger);
		ClickSendText(AddOrganizationUserObject.docissueCountry, data.get("IssueCountry"), logger);
		ClickSendText(AddOrganizationUserObject.Nationality, data.get("Nationality"), logger);
		SelectDropdownByValue(AddOrganizationUserObject.genderDropdown , data.get("Gender"), logger);
		selectCalendarDate(AddOrganizationUserObject.DOBcalIcon, GeneralObjects.yearDD, GeneralObjects.monthDD, GeneralObjects.dateObj, data.get("DOB"), logger);
		selectCalendarDate(AddOrganizationUserObject.DocExpCalIcon, GeneralObjects.yearDD, GeneralObjects.monthDD, GeneralObjects.dateObj, data.get("ExpDate"), logger);


}

	public void add_Doc_Action( ExtentTest logger ) throws InterruptedException {
     //click(AddOrganizationUserObject.adddocumentBtn);
     SubmitButton(SearchOrganizationUser.updateDocSaveBtn, logger);
	}

	public void verify_doc_Rows(Map<String, String> data, ExtentTest logger) {


		String [] docDetails = {data.get("DocumentType"), data.get("DocNo"),data.get("IssueCountry"),data.get("ExpDate"),data.get("DOB"),data.get("Nationality"),  data.get("Gender")  } ;
		verifyElementInTableRow(AddOrganizationUserObject.docRow, docDetails, logger);

	}

	public void resetAll_doc_Action( ExtentTest logger ) throws InterruptedException {
		SubmitButton(AddOrganizationUserObject.docResetAllbtn, logger);
		}

	public void Search_User(ExtentTest logger) {
		 SubmitButton(GeneralObjects.searchBtn, logger);
		 click(SearchOrganizationUser.searchResultRow, logger);
		 SubmitButton(SearchOrganizationUser.viewBtn, logger);
	}

public void validate_resettedField(ExtentTest logger) throws InterruptedException {

		verifyElementText(AddOrganizationUserObject.documentTypeDropdown, "Alien Registration", "Field value is as expected" , logger);
        verifyElementText(AddOrganizationUserObject.docissueCountry, "", "Field value is as expected" , logger);
		verifyElementText(AddOrganizationUserObject.DocExpCalIcon, "", "Field value is as expected" , logger);
		verifyElementText(AddOrganizationUserObject.DOBcalIcon, "", "Field value is as expected" , logger);
		verifyElementText(AddOrganizationUserObject.Nationality, "", "Field value is as expected" , logger);
		verifyElementText(AddOrganizationUserObject.genderDropdown, "Male", "Field value is as expected" , logger);
		}

public void update_User(ExtentTest logger) throws InterruptedException {

	 SubmitButton(SearchOrganizationUser.saveUserBtn, logger);
	 SubmitButton(SearchOrganizationUser.OKBtn, logger);
	 verifyElementText(SearchOrganizationUser.SuccessfulupdateMsg, "IB1214I: User details have been updated successfully.", "Update user popup is displaying " ,  logger);
	 SubmitButton(SearchOrganizationUser.updatepopupOKBtn, logger);
     SubmitButton(SearchOrganizationUser.CloseBtn, logger);
}

public void delete_doc(ExtentTest logger) {
	click(AddOrganizationUserObject.docRow, logger);
	SubmitButton(SearchOrganizationUser.editBtn, logger);
	SubmitButton(SearchOrganizationUser.deleteDocIcon, logger);
	elementNotDisplayed(AddOrganizationUserObject.docRow, logger);

}




}