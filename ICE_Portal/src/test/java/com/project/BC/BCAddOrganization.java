package com.project.BC;


import java.io.IOException;
import java.util.Date;
import java.util.Map;

import com.project.pages.AddOrganizationObject;
import com.project.pages.AddOrganizationUserObject;
import com.project.pages.GeneralObjects;
import com.project.pages.AttachDocumentObjects;
import com.project.pages.SearchOrganizationUser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCAddOrganization extends TestBase {

	public void navigate_To_AddOrganization(ExtentTest logger ) throws IOException {

		mouseHoveronElementAndClick("//a/span[contains(text(),'"+getLocalData("menu.ice.portal", data)+"')]");
		//mouseHoveronElementAndClick(GeneralObjects.linkIcePortal);
        mouseHoveronElementAndClick(GeneralObjects.linkAddOrganization);
       // mouseHoveronElementAndClick("//a/span[contains(text(),'"+getLocalData("ice.add.organisation")+"')]");
		}

	public void Enter_Organization_Details(Map<String, String> data, ExtentTest logger ) throws InterruptedException {
		click(AddOrganizationObject.MechanismType, logger);
		ClickSendText1(AddOrganizationObject.OrganizationName, data.get("OrganizationName"), logger);
		ClickSendText1(AddOrganizationObject.OrganizationLicenseNo, data.get("OrganizationLicense"), logger);
		ClickSendText1(AddOrganizationObject.ParentOrganization, data.get("ParentOrganizationName"), logger);
		ClickSendText1(AddOrganizationObject.NoOfBranches, data.get("NoOfBranches"), logger);

}

	public void Enter_Organization_Address(Map<String, String> data, ExtentTest logger ) throws InterruptedException {

		ClickSendText1(AddOrganizationObject.ZoneName, data.get("ZoneName"), logger);
		ClickSendText1(AddOrganizationObject.StreetName, data.get("StreetName"), logger);
		ClickSendText1(AddOrganizationObject.countryState, data.get("countryState"), logger);
		ClickSendText1(AddOrganizationObject.Location, data.get("Location"), logger);

}
	public void Enter_Org_OwnerDetails(Map<String, String> data, ExtentTest logger ) throws InterruptedException {

		ClickSendText1(AddOrganizationObject.OwnerName, data.get("OwnerName"), logger);
		ClickSendText1(AddOrganizationObject.OwnerFamilyName, data.get("OwnerFamilyName"), logger);
		ClickSendText1(AddOrganizationObject.OwnerPhoneNo, data.get("PhoneNo"), logger);
		ClickSendText1(AddOrganizationObject.OwnerEmail, data.get("Email"), logger);

}


	public void Enter_Org_GMDetails(Map<String, String> data, ExtentTest logger ) throws InterruptedException {

		ClickSendText1(AddOrganizationObject.GMName, data.get("GMName"), logger);
		ClickSendText1(AddOrganizationObject.GMFamilyName, data.get("GMFamilyName"), logger);
		ClickSendText1(AddOrganizationObject.GMPhoneNo, data.get("PhoneNo"), logger);
		ClickSendText1(AddOrganizationObject.GMEmail, data.get("Email"), logger);

}

	public void Enter_Org_RepresentativeDetails(Map<String, String> data, ExtentTest logger ) throws InterruptedException {

		ClickSendText1(AddOrganizationObject.RepName, data.get("RepName"), logger);
		ClickSendText1(AddOrganizationObject.RepFamilyName, data.get("RepFamilyName"), logger);
		ClickSendText1(AddOrganizationObject.RepPhoneNo, data.get("PhoneNo"), logger);
		ClickSendText1(AddOrganizationObject.RepEmail, data.get("Email"), logger);

}

public void add_Document(Map<String, String> data, ExtentTest logger ) throws InterruptedException {


		click(AddOrganizationUserObject.addDocPlusBtn, logger);

		//SelectDropdownByVisibleText(AddOrganizationUserObject.documentTypeDropdown, data.get("DocumentType"), logger);
		SelectDropdownByValue(AddOrganizationUserObject.documentTypeDropdown, data.get("DocumentType").toUpperCase().trim().replace(' ','_' ), logger);
		ClickSendText1(AddOrganizationUserObject.documentNo, data.get("DocNo"), logger);
		ClickSendText1(AddOrganizationUserObject.docissueCountry, data.get("IssueCountry"), logger);
		ClickSendText1(AddOrganizationUserObject.Nationality, data.get("Nationality"), logger);
		SelectDropdownByValue(AddOrganizationUserObject.genderDropdown , data.get("Gender"), logger);
		selectCalendarDateMultiLang(AddOrganizationObject.DOBcalIcon, GeneralObjects.yearDD, GeneralObjects.monthDD, GeneralObjects.dateObj, data.get("DOB"), logger);
		selectCalendarDateMultiLang(AddOrganizationObject.DocExpCalIcon, GeneralObjects.yearDD, GeneralObjects.monthDD, GeneralObjects.dateObj, data.get("ExpDate"), logger);


}



	public void add_Organization(ExtentTest logger) throws IOException  {
		 SubmitButton(AddOrganizationObject.AddOrgBtn, logger);
		 verifyElementText(AddOrganizationObject.AddorgDialoge, getLocalData("add.successful",data), "Added user popup is displaying " ,  logger);

	}
	public void No_AdditionalOrg(ExtentTest logger) throws IOException {
		  click(AddOrganizationUserObject.noBtn,  logger);
		 //SubmitButton(AddOrganizationUserObject.noBtn, logger);
		 verifyElementDisplayed("//h1[@class='pageTitle' and contains(text(),'"+getLocalData("menu.ice.search.organisation",data).trim()+"')]", logger);

	}





}