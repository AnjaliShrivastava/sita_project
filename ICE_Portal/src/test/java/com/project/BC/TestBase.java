package com.project.BC;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeClass;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Keys;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ISuite;
import org.testng.ITest;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

//import jxl.Workbook;

public class TestBase {
	protected String usedDeviceName;
	protected String testname;
	public static Map<String, String> data = null;
	protected Throwable exception = null;
	public static RemoteWebDriver driverinstance;
	public static DesiredCapabilities dc;
	public int const_waitlow = 20;
	protected int const_waitmed = 60;
	protected int const_waithigh = 240;
	public static ExtentReports extent;
	public String testCaseName;
	public static ExtentTest logger;
	public static ThreadLocal<WebDriver> driver;
	public WebDriver instances;
	public static ThreadLocal<ExtentTest> testReport;


	Date date = new Date();
	SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyy");
	String strDate = formatter.format(date);

	// ****************** WEBDRIVER Methods *************************** //

	public void LaunchBrowser(Map<String, String> data) throws MalformedURLException, InterruptedException {
		try {
			driver = new ThreadLocal<WebDriver>();

			// switch (getProperty("browser.name").toUpperCase()) {
			switch (data.get("Browser").toUpperCase()) {
			case ("CHROME"):

				if (data.get("Arabian").equalsIgnoreCase("Yes")) {
					System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--lang=ar");
					driverinstance = new ChromeDriver(options);
					driver.set(driverinstance);
					dc = new DesiredCapabilities();
				} else {
					System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
					driverinstance = new ChromeDriver();
					driver.set(driverinstance);
				}

				break;

			case ("FIREFOX"):
				System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");

				if (data.get("Arabian").equalsIgnoreCase("Yes")) {
					FirefoxOptions firefoxOptions = new FirefoxOptions();
					FirefoxProfile profile = new FirefoxProfile();
					firefoxOptions.setCapability("marionette", true);
					profile.setPreference("intl.accept_languages", "ar");
					firefoxOptions.setProfile(profile);
					driverinstance = new FirefoxDriver(firefoxOptions);
				} else {
					FirefoxOptions firefoxOptions = new FirefoxOptions();
					firefoxOptions.setCapability("marionette", true);
					driverinstance = new FirefoxDriver(firefoxOptions);
				}
				break;

			case ("IE"):

				if (data.get("Arabian").equalsIgnoreCase("Yes")) {
					System.setProperty("webdriver.ie.driver", "./Drivers/IEDriverServer.exe");
					dc = new DesiredCapabilities();
					dc.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
					dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
					dc.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true);
					dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					driverinstance = new InternetExplorerDriver(dc);
					driver.set(driverinstance);

				} else {
					System.setProperty("webdriver.ie.driver", "./Drivers/IEDriverServer.exe");
					dc = new DesiredCapabilities();
					dc.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
					dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
					dc.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true);
					dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					driverinstance = new InternetExplorerDriver(dc);
					driver.set(driverinstance);

				}

				break;
			}
		} catch (Exception e) {

		}

	}

	public void LaunchBrowserParallely(Map<String, String> data) throws MalformedURLException, InterruptedException {
		try {
			driver = new ThreadLocal<WebDriver>();

			// switch (getProperty("browser.name").toUpperCase()) {
			switch (data.get("Browser").toUpperCase()) {
			case ("CHROME"):
				// System.setProperty("webdriver.chrome.driver", "/Drivers/chromedriver.exe");
				// driver = new ChromeDriver();
				// For Japenese
				if (data.get("JapeneseVersion").equalsIgnoreCase("Yes")) {
					System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--lang=ja");
					driverinstance = new ChromeDriver(options);
					driver.set(driverinstance);

				} else {
					System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
					/*
					 * driverinstance = new ChromeDriver(); driver.set(driverinstance);
					 */
					URL url = new URL("http://172.25.79.71:4444/wd/hub");
					dc = new DesiredCapabilities();
					dc.setBrowserName("Chrome");
					dc.setPlatform(Platform.WINDOWS);
					driverinstance = new RemoteWebDriver(url, dc);
					driver.set(driverinstance);
					System.out.println("This is grid exceution");
				}

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@BeforeSuite
	public void setUp() {

		extent = new ExtentReports("./TestReports/Report" + strDate + ".html", false);

	}

	@BeforeMethod
	public void startTest(Method method) {

		try {
			testname = method.getName();
			getTestData();

			if (data.get("Execute").equalsIgnoreCase("yes")) {
				LaunchBrowser(data);
			}

			driver.get().manage().window().maximize();
			driver.get().manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			driver.get().manage().deleteAllCookies();
			driver.get().navigate().to(getProperty("URL"));
			if (data.get("Browser").equals("IE")) {
				driver.get().navigate().to("javascript:document.getElementById('overridelink').click()");
			}
			driver.get().manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

			logger = extent.startTest(method.getName());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@AfterMethod
	public void tearDown(Method method) {
		try {
			if (data.get("Execute").equalsIgnoreCase("yes")) {

				driver.get().close();
				extent.endTest(logger);
				extent.flush();

			}
		} catch (Exception e) {

		}
	}

	public void takeSnap() {

		String timeStamp = new SimpleDateFormat("DDMMMYY_HHMMSS").format(Calendar.getInstance().getTime());

		try {
			File file = new File("./TestReports/ScreenShots/" + strDate + "/" + timeStamp + ".jpg");
			FileUtils.copyFile(((TakesScreenshot) driver.get()).getScreenshotAs(OutputType.FILE),
					new File("./TestReports/ScreenShots/" + strDate + "/" + timeStamp + ".jpg"));
		} catch (WebDriverException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		} catch (IOException e) {
			logger.log(LogStatus.WARNING, e.getMessage());
		}
	}

	public List<String> GetResponse(String urlvalue, String jsonxpath) throws Exception {
		String urlPath = getProperty("apiurl") + urlvalue;
		String headervalue = getProperty("apitoken");
		System.out.println(urlPath);
		String jsonData;
		List<String> values = null;
		URL url = new URL(urlPath);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("accept", "application/json");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Authorization", headervalue);

		try {
			System.out.println(con.getResponseCode());
			if (con.getResponseCode() == 200) {

				BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
				jsonData = "";

				if ((jsonData = reader.readLine()) != null) {
					System.out.println(jsonData);
				}

				// Get the specific values from the json
				DocumentContext jsonContext = JsonPath.parse(jsonData);
				values = jsonContext.read(jsonxpath);
				values = new ArrayList<String>(new HashSet<String>(values));
				// values = new ArrayList<String>(values);
				System.out.println("values size = " + values.size());
			} else {
				// client.report("API Json details not extracted", false);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return values;
	}

	public static String getJSONPath(Map<String, String> data) {
		String JSONFilePath = "";
		if (data.get("JapeneseVersion").equalsIgnoreCase("Yes")) {
			JSONFilePath = getProperty("user.dir") + "\\" + getProperty("jsonfilepathJapense");
		} else {
			JSONFilePath = getProperty("user.dir") + "\\" + getProperty("jsonfilepathEnglish");
		}
		return JSONFilePath;

	}

	public static String Gettextvalue(String jsonheader) {
		String JSONValue = "";

		try {
			String UTF8Str = "";
			String s;
			// String filePath = getProperty("user.dir") + "\\"+getProperty("jsonfilepath");

			File file = new File(getJSONPath(data));
			BufferedReader in = new BufferedReader(new FileReader(file));

			while ((s = in.readLine()) != null) {

				if (new String(s.getBytes(), "UTF-8") != null)
					UTF8Str = UTF8Str + (new String(s.getBytes(), "UTF-8"));
			}
			JSONParser arabicParser = new JSONParser();
			JSONObject obj = (JSONObject) arabicParser.parse(UTF8Str);
			JSONObject jObject = new JSONObject(obj);
			JSONValue = (String) jObject.get(jsonheader);
			in.close();
			return JSONValue;
		} catch (IOException e) {
			System.out.println(e.getMessage());

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return JSONValue;

	}
	/*
	 * public static String Gettextvalue(String jsonheader) { String filePath =
	 * getProperty("user.dir") + "\\"+getProperty("jsonfilepath"); String jsonvalue
	 * = null; // Get the specific values from the json language file JSONParser
	 * parser = new JSONParser();
	 *
	 * try { File fileDir = new File(filePath); BufferedReader in = new
	 * BufferedReader( new InputStreamReader( new FileInputStream(fileDir),
	 * "UTF8")); Object obj = parser.parse(in);
	 *
	 *
	 *
	 * JSONObject jsonObject = (JSONObject) obj;
	 *
	 * jsonvalue = (String) jsonObject.get(jsonheader); if (jsonvalue.isEmpty()) {
	 * System.out.println("json value not retrieved"); } } catch
	 * (FileNotFoundException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (IOException e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); } catch (ParseException e) { // TODO
	 * Auto-generated catch block e.printStackTrace();
	 * System.out.println(e.getMessage()); }
	 *
	 * System.out.println(jsonvalue);
	 *
	 * return jsonvalue;
	 *
	 * }
	 */

	public void getTestData() throws IOException {
		int z;
		data = new HashMap<String, String>();
		Row row;
		FileInputStream fileInputStream = new FileInputStream(getProperty("data.spreadsheet.name"));
		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
		Sheet dataSheet = workbook.getSheet(getTestName());

		for (z = 1; z < dataSheet.getLastRowNum(); z++) {
			row = dataSheet.getRow(z);
			if (row.getCell(0).getStringCellValue().equalsIgnoreCase(testname)) {
				break;
			}
		}

		for (int i = 0; i < dataSheet.getRow(0).getLastCellNum(); i++) {
			row = dataSheet.getRow(0);
			String key = row.getCell(i).getStringCellValue();
			System.out.println(key);
			row = dataSheet.getRow(z);
			String value = row.getCell(i).getStringCellValue();
			System.out.println(value);
			data.put(key, value);
		}
		workbook.close();
	}

	public static String getProperty(String property) {
		if (System.getProperty(property) != null) {
			return System.getProperty(property);
		}
		File setupPropFile = new File("setup.properties");
		if (setupPropFile.exists()) {
			Properties prop = new Properties();
			FileReader reader;
			try {
				reader = new FileReader(setupPropFile);
				prop.load(reader);
				reader.close();
				return prop.getProperty(property);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public String getTestName() {
		String[] klassNameSplit = this.getClass().getName().split("\\.");
		System.out.println(klassNameSplit[klassNameSplit.length - 1]);
		return klassNameSplit[klassNameSplit.length - 1];

	}

	public void onFinish(ISuite suite) {
		System.out.println("Finishing");
	}

	public void onStart(ISuite suite) {
		System.out.println("Starting");
	}

	public void ClickSendText(String Objname, String Textval, ExtentTest Log) {
		try {
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(Objname)));

			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
				driver.get().findElement(By.xpath(Objname)).click();
				driver.get().findElement(By.xpath(Objname)).clear();
				driver.get().findElement(By.xpath(Objname)).sendKeys(Textval);

				// Log.log(LogStatus.PASS, Textval + " entered in the text field ");
				// takeSnap();

			} else {
				Log.log(LogStatus.FAIL, Objname + " not dipslayed on page");
				System.out.println(Objname + "is not displayed");
				// takeSnap();

			}
		} catch (NoSuchElementException e) {

			Log.log(LogStatus.FAIL, "Unable to enter the " + Textval);
			// takeSnap();
		}
	}

	public boolean elementAvailbility(String Objname, String object, ExtentTest logger) {
		try {
			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
				logger.log(LogStatus.INFO, driver.get().findElement(By.xpath(Objname)).getText() + " is displayed");
				driver.get().findElement(By.xpath(object)).click();
				takeSnap();

			}
			return true;
		} catch (WebDriverException e) {
			logger.log(LogStatus.INFO, " Password Alert is displayed");

		}
		return false;
	}

	public boolean verifyElementDisplayedlongTime(String Objname, String message, ExtentTest logger) {
		// Thread.sleep(2000);
		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), 300);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			takeSnap();
			System.out.println((driver.get().findElement(By.xpath(Objname)).getText() + "-" + message));
			// logger.log(LogStatus.PASS,driver.get().findElement(By.xpath(Objname)).getText()
			// + " is dipslayed on page");
			return true;
		} else {
			logger.log(LogStatus.FAIL, Objname + "is not displayed");
			System.out.println(Objname + "is not displayed");
			// takeSnap();
			return false;
		}
	}

	public void verifyElementDisplayed(String Objname, ExtentTest logger) {

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), 60);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			takeSnap();
			logger.log(LogStatus.PASS, driver.get().findElement(By.xpath(Objname)).getText() + "  page is displaying");

		} else {
			logger.log(LogStatus.FAIL, Objname + "is not displayed");
			System.out.println(Objname + "is not displayed");

		}
	}

	public boolean verifyErroDisplayed(String Objname) {

		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			System.out.println((driver.get().findElement(By.xpath(Objname)).getText()));
			takeSnap();
			return true;
		} else {
			System.out.println(Objname + "is not displayed");
			takeSnap();
			return false;
		}
	}

	public void verifyElementText(String Objname, String Text,String message, ExtentTest logger) {
		try {
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));
			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
				if (driver.get().findElement(By.xpath(Objname)).getText().equals(Text)) {
					logger.log(LogStatus.PASS, Text + " : " + message);
					takeSnap();
				}else {

				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Text displayed is not expected" + e.getMessage());
			takeSnap();
		}

	}

	public boolean verifyElementcontainsText(String Objname, String Text, String message, ExtentTest logger) {

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));
		if (driver.get().findElement(By.xpath(Objname)).getText().contains(Text)) {
			logger.log(LogStatus.PASS, Text + message);
			return true;

		} else {
			System.out.println("Unsuccessful Completion");
			logger.log(LogStatus.FAIL, "Unsuccessful Completion");
			return false;
		}
	}

	public boolean verifyElementinContainerContainsText(String Objname, String strText, ExtentTest logger) {
		try {
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {

				logger.log(LogStatus.PASS, Objname + "is displayed");

				List<WebElement> totalObj = driver.get().findElements(By.xpath(Objname));
				for (int i = 0; i < totalObj.size(); i++) {
					if (totalObj.get(i).getText().contains(strText)) {
						System.out.println(strText + " is dispalyed inside container");
						takeSnap();
						logger.log(LogStatus.PASS, strText + " is dispalyed inside container");
					} else {
						System.out.println(strText + " not dispalyed inside container");
						takeSnap();
						logger.log(LogStatus.FAIL, strText + " is not dispalyed inside container");
					}
				}
				return true;

			}
		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			takeSnap();
		}
		return false;

	}

	public void verifyElementinContainer(String Objname, String strText, ExtentTest logger) {
		try {

			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {

				logger.log(LogStatus.PASS, Objname + "is displayed");

				List<WebElement> totalObj = driver.get().findElements(By.xpath(Objname));
				for (int i = 0; i < totalObj.size(); i++) {
					if (totalObj.get(i).getText().equalsIgnoreCase(strText)) {
						System.out.println(strText + " is dispalyed inside container");
						takeSnap();
						logger.log(LogStatus.PASS, strText + " is dispalyed inside container");
					} else {
						System.out.println(strText + " not dispalyed inside container");
						takeSnap();
						logger.log(LogStatus.FAIL, strText + " is not dispalyed inside container");
					}
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			takeSnap();
		}

	}

	public boolean verifyElementSelected(String Objname, String message, ExtentTest logger)
			throws InterruptedException {

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

		if (driver.get().findElement(By.xpath(Objname)).isEnabled()) {
			System.out.println((driver.get().findElement(By.xpath(Objname)).getText() + "-" + message));
			logger.log(LogStatus.PASS, driver.get().findElement(By.xpath(Objname)).getText() + "-" + message);
			takeSnap();
			return true;
		} else {
			System.out.println(Objname + "is not displayed");
			takeSnap();
			logger.log(LogStatus.FAIL, Objname + "is not displayed");
			return false;
		}
	}

	public String GetAttributeValue(String Objname, String attObject) {

		String abc = driver.get().findElement(By.xpath(Objname)).getAttribute(attObject);

		return abc;
	}

	public void ClickSendText1(String Objname, String Textval, ExtentTest logger) {

		try {

			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
				driver.get().findElement(By.xpath(Objname)).click();
				// driver.get().findElement(By.xpath(Objname)).clear();
				driver.get().findElement(By.xpath(Objname)).sendKeys(Textval);
				takeSnap();
				logger.log(LogStatus.PASS, Textval + " successfully entered in text field ");

			} else {
				logger.log(LogStatus.FAIL,
						driver.get().findElement(By.xpath(Objname)).getText() + " is not displayed ");

			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
			takeSnap();
		}

	}

	public void click(String Objname, ExtentTest logger) {
		try {

			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

			if (driver.get().findElement(By.xpath(Objname)).isDisplayed())

				driver.get().findElement(By.xpath(Objname)).click();
			takeSnap();
			logger.log(LogStatus.PASS, " Button clicked Successfully");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, driver.get().findElement(By.xpath(Objname)).getText()
					+ " Button is not clicked : " + e.getMessage());
			takeSnap();
		}

	}

	void SelectDropDown(String Objname, String objectValue, ExtentTest logger) throws InterruptedException {

		try {

			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));
			logger.log(LogStatus.PASS, driver.get().findElement(By.xpath(Objname)) + " is displayed ");
			takeSnap();
			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {

				driver.get().findElement(By.xpath(Objname)).click();
				logger.log(LogStatus.PASS, driver.get().findElement(By.xpath(Objname)) + " is clicked ");
				Thread.sleep(1000);
				List<WebElement> drodown = driver.get().findElements(By.xpath(objectValue));
				Random r = new Random();

				int index = r.nextInt(drodown.size());
				WebElement value = driver.get().findElement(By.xpath(objectValue + "[" + index + "]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click()", value);
				logger.log(LogStatus.PASS, value + " value is selected ");
				takeSnap();
			}
		} catch (NoSuchElementException e) {
			takeSnap();
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());
		} catch (Exception e) {
			// reportStep("The element: "+objectValue+" can't be selected :", "Fail");
		}
	}

	public boolean dropDownSelection(String Objname, String objectValue, String dropDownValue, ExtentTest logger) {

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));
		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			/*
			 * takeSnap(); Thread.sleep(3000);
			 */
			driver.get().findElement(By.xpath(Objname)).click();
			// logger.log(LogStatus.PASS, "Dropdown is clicked ");

			// Thread.sleep(3000);
			List<WebElement> dropdown = driver.get().findElements(By.xpath(objectValue));
			for (WebElement drpdownOptions : dropdown) {

				if (drpdownOptions.getText().contains(dropDownValue)) {
					drpdownOptions.click();

					// logger.log(LogStatus.PASS, dropDownValue + " is selected ");
					// takeSnap();
					break;
				}
			}

			return true;
		} else {

			logger.log(LogStatus.FAIL, dropDownValue + " is not selected ");
			return false;

		}
	}

	public void SubmitButton(String Objname, ExtentTest logger) {
		try {

			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);

			WebElement el = myWaitVar.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Objname)));

			((JavascriptExecutor) driver.get()).executeScript("arguments[0].click()", el);
			logger.log(LogStatus.PASS, driver.get().findElement(By.xpath(Objname)).getText() + " is clicked");
			// logger.log(LogStatus.PASS, logger.addScreenCapture(imagePath));
		} catch (Exception e) {

		}

	}

	public boolean elementNotDisplayed(String Objname, ExtentTest logger) {
		try {
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(Objname)));
			takeSnap();
			logger.log(LogStatus.PASS, Objname + " is not displayed");
			return true;

		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			takeSnap();
		}
		return false;

	}

	public void scrollPage() {
		JavascriptExecutor js = (JavascriptExecutor) driver.get();
		js.executeScript("window.scrollBy(0,70)");
	}

	public void selectCalanderDate(String Objname, String calobj, String DateData, ExtentTest logger)
			throws InterruptedException {

		boolean dateSelected = false;

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
		Thread.sleep(4000);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(Objname)));
		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			driver.get().findElement(By.xpath(Objname)).click();
			takeSnap();
			logger.log(LogStatus.PASS, "Calander is clicked");
			Thread.sleep(4000);
			WebElement dateWidget = driver.get().findElement(By.xpath(calobj));
			List<WebElement> rows = dateWidget.findElements(By.tagName("tr"));
			for (WebElement TetData : rows) {

				List<WebElement> columns = TetData.findElements(By.tagName("td"));

				// comparing the text of cell with today's date and clicking it.
				for (WebElement cell : columns) {
					System.out.println(cell.getText());
					if (cell.getText().equals(DateData)) {
						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy(0,250)");
						Thread.sleep(2000);

						cell.click();
						takeSnap();
						logger.log(LogStatus.PASS, DateData + "  is selected");

						dateSelected = true;
						break;
					} else {

					}
				}
				if (dateSelected == true)
					break;
			}
		} else {
			logger.log(LogStatus.FAIL, "Date is not selected");
			System.out.println("Calander option is not visble");
		}

	}

	public void pageRefresh(String Url) {

		driver.get().get(getProperty("URL"));

	}

	public void verifySortedElementsAssendingOrder(String objectName, String message, ExtentTest logger) {
		try {
			Thread.sleep(4000);

			if ((driver.get().findElement(By.xpath(objectName)).isDisplayed())) {
				logger.log(LogStatus.PASS, objectName + " is Displayed");

				List<WebElement> elements = driver.get().findElements(By.xpath(objectName));

				List<String> textValues = new ArrayList<String>();

				for (int i = 0; i < elements.size(); i++) {

					textValues.add(elements.get(i).getText());
					System.out.println(elements.get(i).getText());
				}
				Collections.sort(textValues);

				List<WebElement> newelemnets = driver.get().findElements(By.xpath(objectName));
				for (int j = 0; j < textValues.size(); j++) {
					if (newelemnets.get(j).getText().equals(textValues.get(j))) {
						System.out.println(textValues.get(j) + message);
						takeSnap();
						logger.log(LogStatus.PASS, textValues.get(j) + message);

					} else {
						takeSnap();
						System.out.println(textValues.get(j) + " is not sorted");
						logger.log(LogStatus.FAIL, textValues.get(j) + " is not sorted");
					}

				}
			} else {
				logger.log(LogStatus.FAIL, objectName + " is not Displayed");
			}
		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			System.out.println(e.getMessage());
		}
	}

	public void verifySortedElementsDecendingOrder(String objectName, String message, ExtentTest logger) {
		try {
			Thread.sleep(4000);
			/*
			 * WebDriverWait myWaitVar = new WebDriverWait(driver.get(),const_waitmed);
			 * myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
			 * objectName)));
			 */
			if ((driver.get().findElement(By.xpath(objectName)).isDisplayed())) {
				logger.log(LogStatus.PASS, objectName + " is Displayed");

				List<WebElement> elements = driver.get().findElements(By.xpath(objectName));

				List<String> textValues = new ArrayList<String>();

				for (int i = 0; i < elements.size(); i++) {

					textValues.add(elements.get(i).getText());
					System.out.println(elements.get(i).getText());
				}
				Collections.sort(textValues);

				Collections.reverse(textValues);

				List<WebElement> newelemnets = driver.get().findElements(By.xpath(objectName));
				for (int j = 0; j < textValues.size(); j++) {
					if (newelemnets.get(j).getText().equals(textValues.get(j))) {
						System.out.println(textValues.get(j) + message);
						takeSnap();
						logger.log(LogStatus.PASS, textValues.get(j) + message);

					} else {
						takeSnap();
						System.out.println(textValues.get(j) + " is not sorted");
						logger.log(LogStatus.FAIL, textValues.get(j) + " is not sorted");
					}

				}
			} else {
				logger.log(LogStatus.FAIL, objectName + " is not Displayed");
			}
		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			System.out.println(e.getMessage());
		}
	}

	public void verifySortedElementsDecendingOrderWithmultipleText(String objectName, String message, int indexValue,
			ExtentTest logger) {
		try {
			Thread.sleep(2000);
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(objectName)));

			List<WebElement> elements = driver.get().findElements(By.xpath(objectName));

			List<String> textValues = new ArrayList<String>();

			for (int i = 0; i < elements.size(); i++) {

				textValues.add(elements.get(i).getText());
				System.out.println(textValues);
			}
			// String[] arr = elements.get().getText().split(" ");
			Collections.sort(textValues);

			Collections.reverse(textValues);

			List<WebElement> newelemnets = driver.get().findElements(By.xpath(objectName));
			for (int j = 0; j < textValues.size(); j++) {

				String[] sortedarr = newelemnets.get(j).getText().split(" ");
				if (sortedarr[indexValue].equals(textValues.get(j))) {
					System.out.println(textValues.get(j) + message);
					logger.log(LogStatus.PASS, sortedarr[indexValue] + message);
					takeSnap();
				} else {
					logger.log(LogStatus.FAIL, textValues.get(j) + " is not sorted");
					System.out.println(textValues.get(j) + " is not sorted");
					takeSnap();
				}

			}
		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			System.out.println(e.getMessage());
		}
	}

	public static String getElementText(String object, ExtentTest logger) throws InterruptedException {

		String text = "";
		List<WebElement> elements = driver.get().findElements(By.xpath(object));
		if (!elements.isEmpty()) {
			Random r = new Random();
			int index = r.nextInt(elements.size());
			text = elements.get(index).getText();
			System.out.println(text + "has been retrived");
			return text;

		} else {
			logger.log(LogStatus.INFO, text + " is not been retrived");
			return null;
		}
	}

	public static String[] arrayElementText(String object, ExtentTest logger) throws InterruptedException {

		String[] text = {};
		List<WebElement> elements = driver.get().findElements(By.xpath(object));
		if (!elements.isEmpty()) {
			int index;
			Random r = new Random();
			if (elements.size() > 0) {
				index = r.nextInt(elements.size());
			} else {
				index = 0;
			}
			Thread.sleep(1000);
			String str = elements.get(index).getText();
			text = str.split("\n");
			System.out.println(text);

			logger.log(LogStatus.INFO, text + " is been retrived");
			return text;

		} else {
			logger.log(LogStatus.INFO, text + " is not been retrived");
			System.out.println(text);
			return text;

		}
	}

	public void writeInTextFile(String PortID) {
		FileOutputStream fop = null;
		File file;
		try {

			file = new File("D:\\newfile.txt");
			fop = new FileOutputStream(file);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			// get the content in bytes
			byte[] contentInBytes = PortID.getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void selectValueUsingKey(String xpth, String txt, ExtentTest logger) throws InterruptedException {

		driver.get().findElement(By.xpath(xpth)).sendKeys(txt);
		Thread.sleep(2000);
		driver.get().findElement(By.xpath(xpth)).sendKeys(Keys.ARROW_DOWN);
		driver.get().findElement(By.xpath(xpth)).sendKeys(Keys.ENTER);

	}

	public void mouseHoveronElementAndClick(String element1) {
		try {
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waithigh);

			myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(element1)));
			Actions action = new Actions(driver.get());
			action.moveToElement(driver.get().findElement(By.xpath(element1))).click().build().perform();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public void SelectDropdownByValue(String drpdown, String Value, ExtentTest logger) {
		try {
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waithigh);
			myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(drpdown)));
			Select dp = new Select(driver.get().findElement(By.xpath(drpdown)));
			dp.selectByValue(Value);
			takeSnap();
			logger.log(LogStatus.PASS, Value + ": has been selected");
		} catch (Exception e) {
			Assert.assertFalse(false);
			takeSnap();
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void SelectDropdownByVisibleText(String drpdown, String text, ExtentTest logger) {
		try {
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waithigh);
			myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(drpdown)));
			Select dp = new Select(driver.get().findElement(By.xpath(drpdown)));
			dp.selectByVisibleText(text);

			logger.log(LogStatus.PASS, text + ": has been selected");
			takeSnap();
		} catch (Exception e) {
			takeSnap();
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void selectCalendarDate(String CalObj, String yearObj, String monthObj, String DateObj, String date,
			ExtentTest logger) throws InterruptedException {
		try {
			String month = date.substring(3, 5);
			Map<String, String> monthMap = new HashMap<String, String>();
			monthMap.put("01", "Jan");
			monthMap.put("02", "Feb");
			monthMap.put("03", "Mar");
			monthMap.put("04", "Apr");
			monthMap.put("05", "May");
			monthMap.put("06", "Jun");
			monthMap.put("07", "Jul");
			monthMap.put("08", "Aug");
			monthMap.put("09", "Sep");
			monthMap.put("10", "Oct");
			monthMap.put("11", "Nov");
			monthMap.put("12", "Dec");

			boolean dateSelected = false;
			String d = null;

			if (Integer.parseInt(date.substring(0, 2)) < 10) {
				d = date.substring(1, 2);

			} else {
				d = date.substring(0, 2);
			}
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);

			myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(CalObj)));

			if (driver.get().findElement(By.xpath(CalObj)).isDisplayed()) {
				driver.get().findElement(By.xpath(CalObj)).click();

				Select yearDD = new Select(driver.get().findElement(By.xpath(yearObj)));
				yearDD.selectByVisibleText(date.substring(6));

				Select monthDD = new Select(driver.get().findElement(By.xpath(monthObj)));
				monthDD.selectByVisibleText(monthMap.get(month));

				WebElement dateWidget = driver.get().findElement(By.xpath(DateObj));

				List<WebElement> rows = dateWidget.findElements(By.tagName("tr"));

				for (WebElement TetData : rows) {
					List<WebElement> columns = TetData.findElements(By.tagName("td"));

					// comparing the text of cell with today's date and clicking it. for
					for (WebElement cell : columns) {

						if (cell.getText().equals(d)) {
							/*
							 * JavascriptExecutor js = (JavascriptExecutor) driver;
							 * js.executeScript("window.scrollBy(0,250)");
							 */
							cell.click();
							dateSelected = true;
							logger.log(LogStatus.PASS, d + " : is been selected from Calander");
							break;
						}
						if (dateSelected == true)
							break;
					}
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Unable to Select Calander Date :: " + e.getMessage());

		}
	}


	public void selectCalendarDateMultiLang(String CalObj, String yearObj, String monthObj, String DateObj, String date,
			ExtentTest logger) throws InterruptedException {
		try {
			String month = date.substring(3, 5);
			Map<String, String> monthMap = new HashMap<String, String>();
			monthMap.put("01", "0");
			monthMap.put("02", "1");
			monthMap.put("03", "2");
			monthMap.put("04", "3");
			monthMap.put("05", "4");
			monthMap.put("06", "5");
			monthMap.put("07", "6");
			monthMap.put("08", "7");
			monthMap.put("09", "8");
			monthMap.put("10", "9");
			monthMap.put("11", "10");
			monthMap.put("12", "11");

			boolean dateSelected = false;
			String d = null;

			if (Integer.parseInt(date.substring(0, 2)) < 10) {
				d = date.substring(1, 2);

			} else {
				d = date.substring(0, 2);
			}
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);

			myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(CalObj)));

			if (driver.get().findElement(By.xpath(CalObj)).isDisplayed()) {
				driver.get().findElement(By.xpath(CalObj)).click();

				Select yearDD = new Select(driver.get().findElement(By.xpath(yearObj)));
				yearDD.selectByVisibleText(date.substring(6));

				Select monthDD = new Select(driver.get().findElement(By.xpath(monthObj)));
				monthDD.selectByValue(monthMap.get(month));

				WebElement dateWidget = driver.get().findElement(By.xpath(DateObj));

				List<WebElement> rows = dateWidget.findElements(By.tagName("tr"));

				for (WebElement TetData : rows) {
					List<WebElement> columns = TetData.findElements(By.tagName("td"));

					// comparing the text of cell with today's date and clicking it. for
					for (WebElement cell : columns) {

						if (cell.getText().equals(d)) {
							/*
							 * JavascriptExecutor js = (JavascriptExecutor) driver;
							 * js.executeScript("window.scrollBy(0,250)");
							 */
							cell.click();
							dateSelected = true;
							logger.log(LogStatus.PASS, d + " : is been selected from Calander");
							break;
						}
						if (dateSelected == true)
							break;
					}
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Unable to Select Calander Date :: " + e.getMessage());

		}
	}


	public void verifyElementInTableRow(String Objname, String[] strText, ExtentTest logger) {
		try {

			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {

				List<WebElement> totalObj = driver.get().findElement(By.xpath(Objname)).findElements(By.tagName("td"));
				for (int i = 0; i < strText.length; i++) {
					if (totalObj.get(i).getText().equals(strText[i])) {

						takeSnap();
						logger.log(LogStatus.PASS, strText[i] + " : dispalyed inside container");
					} else {
						takeSnap();
						logger.log(LogStatus.FAIL, strText[i] + " : is not displaying inside container");
					}
				}
			}
		} catch (Exception e) {
			takeSnap();
			logger.log(LogStatus.FAIL, e.getMessage());

		}
	}

	public void Upload_File(String Objname, String filePath, ExtentTest logger) {

		try {

			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

			WebElement uploadElement = driver.get().findElement(By.xpath(Objname));
			uploadElement.sendKeys(filePath);
			takeSnap();
			logger.log(LogStatus.PASS, filePath + " has been attached Successfully ");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
			takeSnap();
		}

	}


	  public static String getLocalData(String key, Map<String,String> data) throws IOException  {

		String value = null ; int z;

		  List<String> rowValues = new  ArrayList<String>();

	  FileInputStream fileInputStream = new  FileInputStream(getProperty("Localdata.spreadsheet.name"));
	  HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
	  Sheet dataSheet =  workbook.getSheet("Elements");
	 for (z=7999;z<dataSheet.getLastRowNum();z++) {
		rowValues.add(dataSheet.getRow(z).getCell(3).getStringCellValue());
		if(rowValues.contains(key)) {
			if(data.get("Arabian").equalsIgnoreCase("No")) {
				value= dataSheet.getRow(z).getCell(4).getStringCellValue();
				System.out.println(value);
				break;

			}else {
				value= dataSheet.getRow(z).getCell(6).getStringCellValue();
				System.out.println(value);
				break;
			}
		}
	 }

	 workbook.close();

	return value;
	  }

public String reverseDateFormate(String date) {
	String FormattedDate = null ;
	try {
Date d;

	d = new SimpleDateFormat("dd-mm-yyyy").parse(date);
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
FormattedDate = formatter.format(d).toString() ;

} catch (java.text.ParseException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
	return FormattedDate;




}



}




