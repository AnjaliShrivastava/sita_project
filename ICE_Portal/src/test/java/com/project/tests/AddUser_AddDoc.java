package com.project.tests;

import java.io.IOException;
import java.lang.reflect.Method;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.project.BC.BCAddOrganizationUser;
import com.project.BC.BCApplicationLogin;
import com.project.BC.TestBase;
import com.relevantcodes.extentreports.ExtentTest;





public class AddUser_AddDoc extends TestBase {


	//@BeforeMethod
	public void beforeClass(Method method) {
		logger = extent.startTest(method.getName());
	}




@Test
	public void AddDocument_OrganizationUser() throws InterruptedException, IOException{

		BCApplicationLogin LoginPage = new BCApplicationLogin();
		BCAddOrganizationUser user = new BCAddOrganizationUser();
		LoginPage.LogintoApplication(logger) ;
		user.navigate_To_AddOrganizationUser(logger);
		user.add_User_Details(data, logger);
		user.add_Document(data,logger);
		user.add_Doc_Action( logger);
		user.verify_doc_Rows(data,logger);
		user.add_User(logger);
		user.No_AdditionalUser(logger);
	}

	@Test
	public void Add_MasterUser() throws InterruptedException, IOException{

		BCApplicationLogin LoginPage = new BCApplicationLogin();
		BCAddOrganizationUser user = new BCAddOrganizationUser();
		LoginPage.LogintoApplication(logger) ;
		user.navigate_To_AddOrganizationUser(logger);
		user.add_User_Details(data, logger);
		user.add_User(logger);
		user.No_AdditionalUser(logger);
	}

@Test
	public void AddDocument_SystemUser() throws InterruptedException, IOException{

		BCApplicationLogin LoginPage = new BCApplicationLogin();
		BCAddOrganizationUser user = new BCAddOrganizationUser();
		LoginPage.LogintoApplication(logger) ;
		user.navigate_To_AddOrganizationUser(logger);
		user.add_User_Details(data, logger);
		user.add_Document(data,logger);
		user.add_Doc_Action( logger);
		user.verify_doc_Rows(data,logger);
		user.add_User(logger);
		user.No_AdditionalUser(logger);
	}

	@Test
	public void AddDocument_BranchAdmin() throws InterruptedException, IOException{

		BCApplicationLogin LoginPage = new BCApplicationLogin();
		BCAddOrganizationUser user = new BCAddOrganizationUser();
		LoginPage.LogintoApplication(logger) ;
		user.navigate_To_AddOrganizationUser(logger);
		user.add_User_Details(data, logger);
		user.add_Document(data,logger);
		user.add_Doc_Action( logger);
		user.verify_doc_Rows(data,logger);
		user.add_User(logger);
		user.No_AdditionalUser(logger);
	}

	@Test
	public void AddDocument_resetFunctionality() throws InterruptedException, IOException{

		BCApplicationLogin LoginPage = new BCApplicationLogin();
		BCAddOrganizationUser user = new BCAddOrganizationUser();
		LoginPage.LogintoApplication(logger) ;
		user.navigate_To_AddOrganizationUser(logger);
		user.add_User_Details(data, logger);
		user.add_Document(data,logger);
		user.resetAll_doc_Action( logger);
		user.validate_resettedField(logger);

	}



}