package com.project.tests;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.project.BC.BCAddOrganizationUser;
import com.project.BC.BCApplicationLogin;
import com.project.BC.BCAttachDocument;
import com.project.BC.BCSearchOrganizationUser;
import com.project.BC.TestBase;
import com.relevantcodes.extentreports.ExtentTest;





public class AttachDocument extends TestBase {


	//@BeforeMethod
	public void beforeClass(Method method) {
		logger = extent.startTest(method.getName());
	}




	@Test
	public void AddNewBranch_AttachDocument() throws InterruptedException, IOException{

		BCApplicationLogin LoginPage = new BCApplicationLogin();
		BCAttachDocument org = new BCAttachDocument();
		LoginPage.LogintoApplication(logger) ;
		org.navigate_To_SearchOrganizationUser(logger);
		org.SearchOrg_Results(logger);
		org.Enter_Branch_Details(data, logger);
		org.Attach_Document(data,logger);
		org.add_Branch_Action(logger);
		org.No_AdditionalBranch(logger);

	}



}