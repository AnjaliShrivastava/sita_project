package com.project.tests;

import java.lang.reflect.Method;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.project.BC.BCAddOrganizationUser;
import com.project.BC.BCApplicationLogin;
import com.project.BC.BCSearchOrganizationUser;
import com.project.BC.TestBase;
import com.relevantcodes.extentreports.ExtentTest;





public class SearchUser_AddDoc extends TestBase {


	//@BeforeMethod
	public void beforeClass(Method method) {
		logger = extent.startTest(method.getName());
	}




	@Test
	public void SearchUser_EditDocument() throws InterruptedException{

		BCApplicationLogin LoginPage = new BCApplicationLogin();
		BCSearchOrganizationUser user = new BCSearchOrganizationUser();
		LoginPage.LogintoApplication(logger) ;
		user.navigate_To_SearchOrganizationUser(logger);
		user.Enter_Search_Details(data, logger);
		user.Search_User(logger);
		user.Edit_Document(data,logger);
		user.add_Doc_Action(logger);
		user.verify_doc_Rows(data, logger);
		user.update_User(logger);

	}


	 @Test
		public void SearchUser_DeleteDocument() throws InterruptedException{

			BCApplicationLogin LoginPage = new BCApplicationLogin();
			BCSearchOrganizationUser user = new BCSearchOrganizationUser();
			LoginPage.LogintoApplication(logger) ;
			user.navigate_To_SearchOrganizationUser(logger);
			user.Enter_Search_Details(data, logger);
			user.Search_User(logger);
			user.delete_doc(logger);
			user.update_User(logger);
	 }
}