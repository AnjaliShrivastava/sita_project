package com.project.tests;

import java.io.IOException;
import java.lang.reflect.Method;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.project.BC.BCAddOrganization;
import com.project.BC.BCAddOrganizationUser;
import com.project.BC.BCApplicationLogin;
import com.project.BC.BCAttachDocument;
import com.project.BC.TestBase;
import com.relevantcodes.extentreports.ExtentTest;





public class AddOrganization extends TestBase {


	//@BeforeMethod
	public void beforeClass(Method method) {
		logger = extent.startTest(method.getName());
	}




	@Test
	public void AddOrganization_ADD_AND_ATTACH_DOC() throws InterruptedException, IOException{

		BCApplicationLogin LoginPage = new BCApplicationLogin();
		BCAddOrganization org = new BCAddOrganization();
		BCAttachDocument attachDoc = new BCAttachDocument();
		BCAddOrganizationUser addDoc = new BCAddOrganizationUser();
		LoginPage.LogintoApplication(logger) ;
		org.navigate_To_AddOrganization(logger);
        org.Enter_Organization_Details(data, logger);
		org.Enter_Organization_Address(data,logger);
		org.Enter_Org_OwnerDetails(data,logger);
		org.Enter_Org_GMDetails(data, logger);
		org.Enter_Org_RepresentativeDetails(data, logger);
		attachDoc.Attach_Document(data, logger);
		org.add_Document(data,logger);
		addDoc.add_Doc_Action( logger);
		addDoc.verify_doc_Rows(data,logger);
		org.add_Organization( logger);
		org.No_AdditionalOrg(logger);



	}





}