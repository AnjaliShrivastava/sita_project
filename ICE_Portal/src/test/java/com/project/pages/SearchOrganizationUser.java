package com.project.pages;

import com.project.BC.TestBase;

public class SearchOrganizationUser extends TestBase {



	//*************  Search **************** //

	public static final String PageTitle ="//h1[@class='pageTitle' and contains(text(),'Search Organisation User')]";

	public static final String txtFamilyName = "//input[@id='familyName']";
	public static final String txtGivenName ="//input[@id='givenName']";
	public static final String txtUserID="//input[@id='userId']";
	public static final String txtOrganizationCode="//input[@id='organisationCode']";
	public static final String txtBranchCode="//input[@id='branchCode']";
	public static final String UserLevelDropDown="//select[@id='iceUserLevel']";
	public static final String AccountStatusDropDown="//select[@id='userAccountStatus']";
	public static final String passwordStatusDropDown="//select[@id='userPasswordStatus']";

    //********************Search Results Row Value *****************************
	 public static final String searchResultRow ="//tbody[@id='searchBranchResult']/tr";


	// search result ....................

		public static final String searchOrgResults ="//tbody[@id='searchBranchResult']/tr[4]";
		public static final String viewBtn ="//button[@id='btnView']";
		public static final String editBtn ="//button[@id='btnEditOrganisationUser']";
		public static final String CloseBtn="//button[@id='btnClose']";

		// Edit Icon.................

		public static final String editDocIcon ="//a/img[@name='editDocumentIcon']";
		public static final String deleteDocIcon ="//a/img[@name='deleteDocumentIcon']";


        // update doc buttons

		public static final String updateDocSaveBtn="//button[@id='updateDocumentBtn']" ;
		public static final String saveUserBtn ="//button[@id='saveButton']";
		public static final String OKBtn="//button[@id='btnPopInOk']";


// succesful Message ****************

		public static final String SuccessfulupdateMsg="//div/div[contains(text(),'IB1214I: User details have been updated successfully.')]";
        public static final String updatepopupOKBtn ="//div[@id='saveEditedOrg' ]/fieldset/div/button";

}

