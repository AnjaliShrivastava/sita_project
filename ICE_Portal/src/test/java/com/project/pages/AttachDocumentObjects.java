package com.project.pages;

import com.project.BC.TestBase;

public class AttachDocumentObjects extends TestBase {



	//*************  Search **************** //

	public static final String searchOrgResults ="//tbody[@id='searchOrganisationResult']/tr";
	public static final String addBranchBtn ="//button[@id='btnAddBranch']";


	// Branch details


	public static final String BranchName ="//input[@id='branchName']";
	public static final String BranchLicenseNo="//input[@id='branch.licenseNumber']";

	public static final String ZoneName="//input[@id='branch.zoneNumber']";
	public static final String StreetNo = "//input[@id='branch.street']" ;
	public static final String LocationSearch="//input[@id='branch.location']";


	public static final String BranchPhoneNO ="//input[@id='branch.phoneNumber']";
	public static final String BranchEmail ="//input[@id='branch.emailAddress']";
	public static final String btnAddFile ="//input[@id='addFile']" ;

	public static final String FirstAttachFile ="//input[@id='files[0]']" ;
	public static final String btnAdd ="//button[@id='add']";


	public static final String SearchBranchTitle ="//h1[@class='pageTitle' and contains(text(),'Search Branches')]" ;




}
