package com.project.pages;

import com.project.BC.TestBase;

public class AddOrganizationUserObject extends TestBase {



	//*************  Search **************** //

	public static final String GivenName ="//input[@id='givenName']";
	public static final String FamilyName ="//input[@id='familyName']";
	public static final String Email="//input[@id='emailAddress']";

	public static final String PhoneNumber="//input[@id='phoneNumber']";
	public static final String Facsimile = "//input[@id='facsimile']";
	public static final String jobTitle="//input[@id='jobTitle']";

	public static final String LocationSearchBtn="//button[@id='issuingLocationSearchId']";

    public static final String userLevelDropDown ="//Select[@id='iceUserLevel']";
	public static final String orgNamelDropDown="//Select[@id='organisationDetails']";
	public static final String branchCodelDropDown="//Select[@id='branchDetails']";



	public static final String addDocPlusBtn="//div[@class='expandControl']/span/button";


	// add document details

	public static final String documentTypeDropdown ="//Select[@id='documentTypeSelectId']";
	public static final String documentNo ="//input[@id='documentNumberId']";
	public static final String genderDropdown ="//Select[@id='genderSelectId']";
	public static final String adddocumentBtn="//button[@id='addDocumentBtn']";
	public static final String docResetAllbtn="//button[@name='reset']";
	public static final String docissueCountry ="//input[@id='documentDetails.documentIssueCountry']";
	public static final String Nationality ="//input[@id='documentDetails.nationality']";


	// DOB details

	public static final String DOBcalIcon ="(//button[@class='ui-datepicker-trigger'])[3]";
	public static final String DocExpCalIcon ="(//button[@class='ui-datepicker-trigger'])[2]";


// Document details Rows  ********************

	public static final String docRow	="//tbody[@id='addDocumentList']/tr[2]";




	public static final String btnSearch ="//button[@id='searchButton']";
	public static final String btnResetAll ="//button[@id='resetButton']";


// Add user Button
	public static final String addUserBtn="//button[@id='btnAdd']";
	public static final String resetuserBtn="//button[@id='reset']";
	public static final String cancelUserBtn="//button[@id='cancel']";

	public static final String noBtn ="//button[@id='btnNo']";
	public static final String yesBtn ="//button[@id='btnYes']";

	// popup messages

	public static final String AddedUserpopup ="//div[@id='popupMessageId']";



}
