package com.project.pages;

import com.project.BC.TestBase;

public class AddOrganizationObject extends TestBase {


	public static final String MechanismType ="//input[@id='organisation.dataSubmissionMechanism2']";;

	//*************  ORGANIZATION DETAILS **************** //

	public static final String OrganizationName ="//input[@id='organisationName']";
	public static final String OrganizationLicenseNo ="//input[@id='licenseId']";
	public static final String ParentOrganization ="//input[@id='organisation.parentOrganisation']";
	public static final String NoOfBranches ="//input[@id='organisation.numberOfAssociatedProperties']";


	//*************  ORGANIZATION ADDRESS **************** //

	public static final String ZoneName="//input[@id='organisation.zoneNumber']";
	public static final String StreetName = "//input[@id='organisation.street']";
	public static final String countryState="//input[@id='organisation.state']";
    public static final String Location="//input[@id='organisation.location']";

    //************************ ORGANIZATION OWNER DETAILS  ************************* //

    public static final String OwnerName  ="//input[@id='organisation.organisationOwner.givenName']";
    public static final String OwnerFamilyName ="//input[@id='organisation.organisationOwner.familyName']";
    public static final String OwnerEmail = "//input[@id='organisation.organisationOwner.emailAddress']";
    public static final String OwnerPhoneNo = "//input[@id='organisation.organisationOwner.phoneNumber']";




  //************************ ORGANIZATION GM DETAILS  ************************* //

    public static final String GMName  ="//input[@id='organisation.generalManager.givenName']";
    public static final String GMFamilyName ="//input[@id='organisation.generalManager.familyName']";
    public static final String GMEmail = "//input[@id='organisation.generalManager.emailAddress']";
    public static final String GMPhoneNo = "//input[@id='organisation.generalManager.phoneNumber']";


  //************************ ORGANIZATION Representative  DETAILS  ************************* //

    public static final String RepName  ="//input[@id='organisation.organisationRepresentative.givenName']";
    public static final String RepFamilyName ="//input[@id='organisation.organisationRepresentative.familyName']";
    public static final String RepEmail = "//input[@id='organisation.organisationRepresentative.emailAddress']";
    public static final String RepPhoneNo = "//input[@id='organisation.organisationRepresentative.phoneNumber']";


// DOB details

	public static final String DOBcalIcon ="(//button[@class='ui-datepicker-trigger']/img)[2]";
	public static final String DocExpCalIcon ="(//button[@class='ui-datepicker-trigger']/img)[1]";


// Add Org Button
	public static final String AddOrgBtn = "//button[@id='search']";
	public static final String pageTitle ="//h1[@class='pageTitle' and contains(text(),'Search Organisation')]";



	public static final String AddorgDialoge ="//div[@id='addOrganisationDialog']/div" ;

}
