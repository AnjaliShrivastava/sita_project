package com.project.pages;

import com.project.BC.TestBase;

public class GeneralObjects extends TestBase {


	// Home Page General Object *******************
	public static final String txtUsername = "//*[@id='username']";
	public static final String txtPassword = "//*[@id='password']";
	public static final String btnLogin = "//*[@id='loginButton']";

	public static final String linkIcePortal = "//a/span[contains(text(),'ICE Portal')]";
	public static final String linkAddOrgUser ="//a[@href='/ice/organisationUser/addOrganisationUser']";
	public static final String linksearchOrg ="//a[@href='/ice/organisation/searchOrganisation']";
    public static final String linksearchOrgUser ="//a[@href='/ice/organisationUser/searchOrganisationUser']";
    public static final String linkAddOrganization  = "//a[@href='/ice/organisation/saveOrganisation']";

	///Component wise ***************

	// Date Object

	public static final String monthDD ="//select[@class='ui-datepicker-month']";
	public static final String yearDD ="//select[@class='ui-datepicker-year']";
	public static final String dateObj ="//table[@class='ui-datepicker-calendar']";

// Buttons ............

	public static final String searchBtn ="//button[@id='searchButton']";
	public static final String resetBtn="//button[@id='resetButton']";




}





